# README taller-git-programacion-4 

Explicación de HTML y CSS sencillo.

## Intro


El código que estamos observando proporciona una descripción concisa de la estructura básica de un documento HTML y, al mismo tiempo, ilustra las diversas formas en las que se puede emplear CSS, dependiendo de las prácticas específicas que se decidan aplicar. Este código es una excelente introducción para entender cómo HTML y CSS interactúan en la construcción de páginas web.

En primer lugar, el código muestra una estructura HTML básica, que comúnmente se conoce como un esqueleto HTML. Esto incluye elementos esenciales como !DOCTYPE html, que declara la versión de HTML que se está utilizando, y las etiquetas html, head, y body. El elemento head es el lugar donde se incluyen metadatos y enlaces a hojas de estilo externas, mientras que el elemento body contiene el contenido visible de la página, como texto, imágenes y otros elementos.

En la parte relacionada con CSS, el código presenta varias opciones para aplicar estilos a elementos HTML. Se muestran tres métodos comunes:

* **Estilos en línea:** En este enfoque, los estilos se aplican directamente a un elemento HTML utilizando el atributo style. Por ejemplo, un párrafo puede tener un color de texto específico o un tamaño de fuente directamente definidos en la etiqueta. Esta es una forma rápida de aplicar estilos, pero puede volverse difícil de mantener en páginas web más grandes.

* **Estilos en la cabecera (head):** Se incluye un bloque de estilo en la sección head del documento HTML. Estos estilos se aplican a todos los elementos seleccionados en el documento. En el código, se utiliza la etiqueta style para definir reglas de estilo CSS que afectan a los elementos dentro del body.

* **Estilos externos:** Este método es el más recomendado para proyectos web más grandes. Se crea un archivo CSS separado, que se enlaza al documento HTML utilizando la etiqueta link. Los estilos definidos en el archivo CSS afectan a todas las páginas que hacen referencia a ese archivo, lo que facilita la consistencia y el mantenimiento de estilos en un sitio web.

La elección del método dependerá de las necesidades del proyecto y de las prácticas recomendadas. Además, el código puede ser un punto de partida para explorar más a fondo las posibilidades de CSS, como la selección de elementos específicos, la creación de diseños complejos y la aplicación de reglas de estilo condicionales.

